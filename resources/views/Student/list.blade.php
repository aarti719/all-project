@extends('layout.template')
@section('content')

<h2>Student List.</h2>

@if($message = session('message'))
	<div class="alert alert-success">{{ session('message')}}</div>
@endif
<a href="{{route('stu.create')}}" class="btn btn-primary">Add New Student</a>

<table class="table">
	<thead>
		<tr>
			<th>S.No</th>
			<th>Name</th>
			<th>Email</th>
			<th>Mobile</th>
			<th>Address</th>
			<th>Action</th>
		</tr>
	</thead>

	<tbody>
		@foreach($students as $index=>$student)
		<tr>
			<td>{{$index+1}}</td>
			<td>{{$student->name}}</td>
			<td>{{$student->email}}</td>
			<td>{{$student->mobile}}</td>
			<td>{{$student->address}}</td>
			<td>
				<a href="{{route('stu.edit',$student->id)}}" class="btn btn-primary">Edit</a>
				<a href="{{route('stu.delete',$student->id)}}" class="btn btn-danger">Delete</a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

<div class="pagination">
	{{$students->links()}}
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.btn-danger').click(function(){
			var conf = confirm('Are you sure you want to delete this record?');
			if(conf)
			{
				return true;
			}
			else
			{
				return false;
			}
		});
	});
</script>
@endsection