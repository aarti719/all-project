@extends('layout.template')
@section('content')

<h2> Create New Student Record</h2>
<form method="post" action="{{route('stu.save')}}">
  {{csrf_field()}}
  <div class="form-group row">
      <label for="name" class="col-sm-2 col-form-label">Name</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="name" name="name">
      </div>
    </div>

    <div class="form-group row">
      <label for="email" class="col-sm-2 col-form-label">Email</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="email" name="email">
      </div>
    </div>

    <div class="form-group row">
      <label for="mobile" class="col-sm-2 col-form-label">Mobile</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="mobile" name="mobile">
      </div>
    </div>

    <div class="form-group row">
      <label for="address" class="col-sm-2 col-form-label">Address</label>
      <div class="col-sm-6">
        <textarea class="form-control" id="address" name="address"></textarea>
      </div>
    </div>

    <button type="submit" class="btn btn-primary">Save</button>
</form>
@endsection