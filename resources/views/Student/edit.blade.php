@extends('layout.template')
@section('content')

<h2> Edit Student Record</h2>
<form method="post" action="{{route('stu.update')}}">
  {{csrf_field()}}
  <input type="hidden" name="id" value="{{$student->id}}">
  <div class="form-group row">
      <label for="name" class="col-sm-2 col-form-label">Name</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="name" name="name" value="{{$student->name}}">
      </div>
    </div>

    <div class="form-group row">
      <label for="email" class="col-sm-2 col-form-label">Email</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="email" name="email" value="{{$student->email}}">
      </div>
    </div>

    <div class="form-group row">
      <label for="mobile" class="col-sm-2 col-form-label">Mobile</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="mobile" name="mobile" value="{{$student->mobile}}">>
      </div>
    </div>

    <div class="form-group row">
      <label for="address" class="col-sm-2 col-form-label">Address</label>
      <div class="col-sm-6">
        <textarea class="form-control" id="address" name="address">value="{{$student->address}}"></textarea>
      </div>
    </div>

    <button type="submit" class="btn btn-primary">Save</button>
</form>
@endsection