@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('acess_denied'))
                     <div class="alert alert-danger" role="alert">
                    {{session('acess_denied')}}
                      </div>
                    @endif
                    

                    @if(Auth::user()->role == 1)
                    <div class="row">
                        <div class="col-md-3">
                            <nav>
                                <ul>
                                    <li>
                                     <a href="{{route('post.list')}}"> 
                                       My Posts</a>
                                    </li>
                                    <li>
                                    <a href="{{route('post.create')}}"> 
                                        New Posts</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
