@extends('layouts.app')
@section('content')
<div class="container">
<h2> Create New Post</h2>

@if (count($errors) > 0 )
<div class="alert alert-danger">
  <ul>
    @foreach($errors ->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<form method="post" action="{{route('post.save')}}" enctype="multipart/form-data">
  {{csrf_field()}}
  <div class="form-group row">
      <label for="name" class="col-sm-2 col-form-label">title</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="title" name="title" value="{{old('title')}}">
      </div>
    </div>

    <div class="form-group row">
      <label for="email" class="col-sm-2 col-form-label">description</label>
      <div class="col-sm-6">
        <textarea class="form-control" id="description" name="description"> {{old('description')}} </textarea>
      </div>
    </div>

     <div class="form-group row">
      <label for="email" class="col-sm-2 col-form-label">image</label>
      <div class="col-sm-6">
        <input type="file" name="image">
      </div>
    </div>

    <button type="submit" class="btn btn-primary">Save</button>
</form>
</div>
@endsection