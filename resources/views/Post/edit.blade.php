@extends('layouts.app')
@section('content')
<div class="container">
<h2> Edit Post Record</h2>
@if (count($errors) > 0 )
<div class="alert alert-danger">
  <ul>
    @foreach($errors ->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<form method="post" action="{{route('post.update')}}">
  {{csrf_field()}}
  <input type="hidden" name="id" value="{{$post->id}}">
  <div class="form-group row">
      <label for="title" class="col-sm-2 col-form-label">Title</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="name" name="title" value="{{$post->title}}">
      </div>
    </div>

    <div class="form-group row">
      <label for="description" class="col-sm-2 col-form-label">Description</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="description" name="description" value="{{$post->description}}">
      </div>
    </div>

    <div class="form-group row">
      <label for="image" class="col-sm-2 col-form-label">Image</label>
      <div class="col-sm-6">
        <input type="file" name="image" value="{{$post->image}}">
      </div>
    </div>

    

    <button type="submit" class="btn btn-primary">Save</button>
</form>
</div>
@endsection