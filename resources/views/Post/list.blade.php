@extends('layout.template')
@section('content')
<div class="container">
<h2>Post List.</h2>

@if($message = session('message'))
	<div class="alert alert-success">{{ session('message')}}</div>
@endif
<a href="{{route('post.create')}}" class="btn btn-primary">Add New Post</a>

<table class="table">
	<thead>
		<tr>
			<th>S.No</th>
			<th>Name</th>
			<th>Alias</th>
			<th>Views</th>
			<th>Created At</th>
			<th>Action</th>
		</tr>
	</thead>

	<tbody>
		@foreach($posts as $index=>$post)
		<tr>
			<td>{{$index+1}}</td>
			<td>{{$post->title}}</td>
			<td>{{$post->alias}}</td>
			<td>{{$post->views}}</td>
			<td>{{$post->created_at}}</td>
			<td>
				<a href="{{route('post.edit',$post->id)}}" class="btn btn-primary">Edit</a>
				<a href="{{route('post.delete',$post->id)}}" class="btn btn-danger">Delete</a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

<div class="pagination">
	{{$posts->links()}}
</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.btn-danger').click(function(){
			var conf = confirm('Are you sure you want to delete this record?');
			if(conf)
			{
				return true;
			}
			else
			{
				return false;
			}
		});
	});
</script>
@endsection