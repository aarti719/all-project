<!DOCTYPE html>
<html>
<head>
	<title>Comment Mail</title>
</head>
<body>

	<p>Dear Admin</p>
	<p>Following post has been posted on '{{$commented_post}}' by {{$commented_by}} on {{$commented_at}}</p>
	<p>{{$comment}}</p>
	<p>
		Regards,<br>
		https://example.com <br>
		contact :9845538719
	</p>

</body>
</html>