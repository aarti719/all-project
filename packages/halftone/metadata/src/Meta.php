<?php
namespace Halftone\Metadata;

class Meta{

	protected $title;
	protected $description;
	protected $keywords;
	public function title($title=null)
	{
		if (!isset($this->title))
		{
			$this->title = $title;
			return true;
		}

		return '<title>'.$this->title. '</title>';
	}

	public function description($description=null){
		if(!isset($this->description))
		{
			$this->description = $description;
			return true;
		} 
		return '<meta name="description" content="'.$this->description.'">';
	}	
	public function keywords($keywords=null){
		if(!isset($this->description))
		{
			$this->keywords = $keywords;
			return true;
		} 
		return '<meta name="keywords" content="'.$this->keywords.'">';
	}	
}	