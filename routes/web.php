<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/','HomeController@index');
Route::get('student/list','StudentController@index')->name('stu.list');
Route::get('student/create', 'StudentController@create')->name('stu.create');
Route::post('student/save', 'StudentController@save')->name('stu.save');
Route::get('student/edit/{id}', 'StudentController@edit')->name('stu.edit');
Route::post('student/update', 'StudentController@update')->name('stu.update');
Route::get('student/delete/{id}', 'StudentController@delete')->name('stu.delete');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('post/{id}','PostController@viewpost')->name('post.view');

Route::group(['middleware' => 'auth'],function(){
	Route::post('post/comment/save','PostController@saveComment')->name('post.comment');
});

Route::group(['middleware' => ['auth','role'],'prefix' => 'admin'], function(){

		Route::get('dashboard','AdminController@dashboard');
		Route::get('post/list', 'PostController@index')->name('post.list');
		Route::get('post/create', 'PostController@create')->name('post.create');
		Route::post('post/save', 'PostController@save')->name('post.save');
		Route::get('post/edit/{id}', 'PostController@edit')->name('post.edit');
		Route::post('post/update', 'PostController@update')->name('post.update');
		Route::get('post/delete/{id}', 'PostController@delete')->name('post.delete');


});
