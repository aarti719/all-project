<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i <=30 ; $i++) 
        {
            DB::table('students')->insert(
    	        	[
    	        		'name'=>'Aarti Bhattrai',
    	        		'email'=>'aarti@gmail.com',
    	        		'mobile'=>561242317,
    	        		'address'=>'Sorakhutte',
                    ]
    	    );
        }    	
    }
}
