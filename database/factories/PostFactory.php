<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Post::class, function (Faker $faker) {
	$title = $faker->sentence;
    return [
        'title' => $title,
        'alias' => str_slug($title),
        'description' => $faker->paragraph,
        'views' => $faker->randomDigit,
        'image' => $faker->image(public_path('/uploads'), 200, 180, 'cats', false, true),
    ];
});
