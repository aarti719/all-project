<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class StudentController extends Controller
{
    public function index()
    {
      $students= Student::paginate(10);
      //dump($students);
    	return view('Student.list',compact('students'));
    }
    public function create()
    {
    	return view('Student.create');	
    }
    public function save(Request $request)
    {
       $student= new Student;
       $student->name =$request->name;
       $student->email =$request->email;
       $student->mobile =$request->mobile;
       $student->address =$request->address;
       $student->save();

       return redirect()->route('stu.list')->with(['message'=>'Record added sucessfully']);
    }

    public function edit($id)
    {
      $student = Student::where ('id',$id)->first();
      return view('Student.edit',compact('student'));
    }

    public function update(Request $request){

        $student = Student::find($request->id);
        $student->name =$request->name;
        $student->email =$request->email;
        $student->mobile =$request->mobile;
        $student->address =$request->address;
        $student->save();

        return redirect()->route('stu.list')->with(['message'=>'Record updated sucessfully']);
    }

    public function delete($id)
    {
       $student = Student::find($id);
       $student->delete();

       return redirect()->route('stu.list')->with(['message'=>'Record has been deleted.']);
    }
}
