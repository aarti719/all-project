<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Comment;
use Auth;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\CommentPosted;
use App\User;
use Meta;
class PostController extends Controller
{
   

    public function index()
    {
      $posts= Post::paginate(10);
      //dump($students);
    	return view('Post.list',compact('posts')
    );
    }
    public function create()
    {
    	return view('Post.create');	
    }
    public function save(PostRequest $request)
    {
       $post= new Post;
       $post->title = $request->title;
       $post->alias = str_slug($request->title);
       $post->description = $request->description;

       if ($file= $request->file('image')){
       	$filename = $file->getClientOriginalName(); 
       	$extension = $file->getClientOriginalExtension() ?: 'png';
       	$folderName = '/uploads/';
       	$contentPath = public_path() . $foldername;
       	$newFileName = str_random(10) . '.' . $extension;
       	$file->move($contentpath, $newFileName);
       	$post->image = $newFileName;
       	$post->image =$request->image;
       }

     
       $post->save();

       return redirect()->route('post.list')->with(['message'=>'Post updated sucessfully']);
    }

    public function edit($id)
    {
      $post = Post::where ('id',$id)->first();
      return view('Post.edit',compact('post'));
    }

    public function update(PostRequest $request){

        $post = Post::find($request->id);
        $post->title =$request->title;
        $post->alias =$request->title;
        $post->description =$request->description;
        $post->save();

        return redirect()->route('post.list')->with(['message'=>'Post updated sucessfully']);
    }

    public function delete($id)
    {
       $student = Post::find($id);
       $student->delete();

       return redirect()->route('post.list')->with(['message'=>'Post has been deleted.']);
    }

    public function viewpost($id)
    {
      try{
              $post = Post::find($id);
            /*if(!$post){
              abort(404);
            }*/
            $comments = Comment::where ('post_id', $post->id)->where('status', 1)->get();
            Meta::title($post->title);
            return view('Post.view',compact('post','comments'));

      }catch(\Exception $e){
        abort(404);

      
      }
    }

    public function saveComment(Request $request)
    {
      $request->validate([
      'comment' => 'required|min:10',
    ]);
      $comment = new Comment;
      $comment->post_id = $request->post_id;
      $comment->user_id = Auth::user()->id;
      $comment->ip_address = $request->ip();
      $comment->comment = $request->comment;
      if ($comment->save() )
      
        $admin = User::where('role',1)->first();
        $post = Post::find($comment->post_id);

        Mail::to($admin)->send(new CommentPosted(Auth::user(), $post , $comment) );
        return redirect()->back()->with('message','comment posted successfully.Your comment is under moderation.');
      }


}

