<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Meta;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts = Post::orderBy('id','desc')->paginate(10);
        Meta::title('Post List');
        Meta::description('Laravel Blog.');
        Meta::keywords('Laravel,blog,post,comment,php');
        return view('home',compact('posts'));
    }
}
