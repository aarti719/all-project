<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CommentPosted extends Mailable
{
    use Queueable, SerializesModels;

    protected $comment_user;
    protected $post;
    protected $comment;




    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $user, $post, $comment )
    {
        $this->comment_user = $user;
        $this->post = $post;
        $this->comment =$comment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@example.com')
        ->view('Mail.comment')
        ->with([
            'commented_by' => $this->comment_user->name,
            'commented_post' => $this->post->title,
            'comment' => $this->comment->comment,
            'commented_at' => $this->comment->created_at,

        ]);
    }
}
